﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="10008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_6_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.6\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_6_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.6\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This library contains the HGF_MenueTools VI's.

Author: H.Brand@gsi.de

History:
Revision 0.0.0.0: Jan 30, 2009 H.Brand@gsi.de; start of development.

Copyright 2009 GSI

Gesellschaft für Schwerionenforschung mbH
Planckstr. 1, 64291 Darmstadt, Germany
Contact: H.Brand@gsi.de

This file is part of the HGF_MenueTools.

    HGF_MenueTools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HGF_MenueTools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with HGF_MenueTools.  If not, see http://www.gnu.org/licenses/.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*+!!!*Q(C=\&gt;5^=BJ"%)&lt;BTSY(4LG"CX,IL+^!33?AS!D\#K1+O5)@A)4S$5A6&lt;.#F4'4I#&amp;R"?G@5+RM(BCL`3)&amp;W0,"]M^PT;(:K,@8DM`2*D_?/Y&lt;@("?/C$?*4\&lt;MV$7-&lt;RTHLP6_E];P'_\W^R$$_/"H`57U94XY&gt;@ZZT/*V@OMD`B_.H$LU@\]@L(D&gt;H._E`X\-@V&amp;Z%^+1(X?GG.N6DEC&gt;ZEC&gt;ZEC&gt;ZE!&gt;ZE!&gt;ZE!?ZETOZETOZETOZE2OZE2OZE2NZ&lt;_1C&amp;\H))374*R-F2:-#S=61F(QEHM34?")00Z6Y%E`C34S*BUO5?"*0YEE]C9@&lt;F(A34_**0)G(5FW3P:(D34S56_!*0)%H]!1?JF4A#1$":%(BI!A-"90"3?!*0)'(5Q7?Q".Y!E`A96C"*`!%HM!4?,CFLUJU47PE?#ADR_.Y()`D=4S5FO.R0)\(]4A?JJ0D=4Q/QJH1+1Z"TEX/"=Y0R_.Y_*,D=4S/R`%Y(I&lt;[%`+_-EX4'DE?QW.Y$)`B-4S5E/%R0)&lt;(]"A?SMLQ'"\$9XA-$V0*]"A?QW.!D%G:8E9RYU&lt;D)C-Q00TLO]8[5YIOM&gt;[EWLSK4;H;&lt;+J.J.I=KI?O?JCKB[2;@.7CKB:,N1CK0U[&amp;6G&amp;5E[BO&lt;B@KS/?"PK.P[7P[CD[HT_B4_K4&gt;_J=P0"[0/BQ/WOVWWG[X7K`87KV7GM`HGMVGGE[HGEQG,[_",\38&amp;],J?WF`N6H=0XR&lt;X.Z^8&gt;QPPS`WS]XV@`C`^!W]'`620^&gt;FD:Y!RZ(RSQ!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="selectMenueEnabled.vi" Type="VI" URL="../selectMenueEnabled.vi"/>
	<Item Name="setMenueEnabled.vi" Type="VI" URL="../setMenueEnabled.vi"/>
	<Item Name="toggleMenueEnabled.vi" Type="VI" URL="../toggleMenueEnabled.vi"/>
</Library>
